package pl.edu.pwr.pp;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	public static char intensityToAscii(int intensity) {
		//dokladna wartosc jednego podprzedzialu
		double podprzedzial=(double) 256.0/INTENSITY_2_ASCII.length();
		
		//konwersja do int zaokragla w dol, np. 127/64=1
		int indeks=(int) (intensity/podprzedzial);
	
		return INTENSITY_2_ASCII.charAt(indeks);
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		int rows=intensities.length;
		int columns=intensities[0].length;
		
		//inicjalizacja tablicy znakow
		char[][] asciiTable =null;
		asciiTable = new char[rows][];
		for (int i=0; i<rows; i++)
		{
			asciiTable[i]=new char[columns];
		}
		
		//zamiana odcieni szarosci na ascii	
		for(int i=0; i<rows; i++)
		{
			for (int j=0; j<columns;j++)
			{
				asciiTable[i][j]=intensityToAscii(intensities[i][j]);
			}
		}
		return asciiTable;
	}

}
