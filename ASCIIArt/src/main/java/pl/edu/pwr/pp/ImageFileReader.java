package pl.edu.pwr.pp;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageFileReader {

	/**
	 * Metoda czyta plik pgm i zwraca tablicę odcieni szarości.
	 * @param fileName nazwa pliku pgm
	 * @return tablica odcieni szarości odczytanych z pliku
	 * @throws URISyntaxException jeżeli plik nie istnieje
	 */
	public int[][] readPgmFile(String fileName) throws URISyntaxException {
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;

		Path path = this.getPathToFile(fileName);
		
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			// w kolejnych liniach kodu można / należy skorzystać z metody
			// reader.readLine()
			
			// pierwsza linijka pliku pgm powinna zawierać P2
			// TODO : Wasz kod
			if(!reader.readLine().equals("P2"))
				System.err.printf("Blad! Zly numer magiczny pliku\n");
			
			
			// druga linijka pliku pgm powinna zawierać komentarz rozpoczynający
			// się od #
			if(!reader.readLine().startsWith("#"))
				System.err.printf("Brak komentarza\n");
			
			// trzecia linijka pliku pgm powinna zawierać dwie liczby - liczbę
			// kolumn i liczbę wierszy (w tej kolejności). Te wartości należy
			// przypisać do zmiennych columns i rows.
			// TODO : Wasz kod
			String temp[]=reader.readLine().split(" "); //rozdziel linijke na dwa stringi z liczbami
			columns=Integer.parseInt(temp[0]); //zamien stringa na inta
			rows=Integer.parseInt(temp[1]);
			
			
			
			// czwarta linijka pliku pgm powinna zawierać 255 - najwyższą
			// wartość odcienia szarości w pliku
			// TODO : Wasz kod
			if(!reader.readLine().equals("255"))
				System.err.printf("Zla maksymalna wartosc szarosci\n");
			
			// inicjalizacja tablicy na odcienie szarości
			intensities = new int[rows][];

			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}

			// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
			// pikseli rozdzielone spacjami
			String line = null;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				String[] elements = line.split(" ");
				for (int i = 0; i < elements.length; i++) {
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					// currentRow i currentColumn są na początku równe zero.
					// Należy je odpowiednio zwiększać, pamiętając o tym, żeby
					// nie wyjść poza zakres tablicy. Plik pgm może mieć w
					// wierszu dowolną ilość liczb, niekoniecznie równą liczbie
					// kolumn.
					// TODO Wasz kod
					currentColumn++;
					if(currentColumn>=columns)
					{
						currentColumn=0;
						currentRow++;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return intensities;
	}
	
	private Path getPathToFile(String fileName) throws URISyntaxException {
		URI uri = ClassLoader.getSystemResource(fileName).toURI();
		return Paths.get(uri);
	}
	
}
