package pl.edu.pwr.pp;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		// np. korzystając z java.io.PrintWriter
		// TODO Wasz kod
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName);
			for(int i=0; i<ascii.length; i++)
			{
				for (int j=0; j<ascii[i].length; j++)
					writer.print(ascii[i][j]+" ");
				writer.println("");
			}
			
			writer.println("");
			writer.println("Wygenerowano przez Generator Obrazów ASCII");
			writer.println("Autorzy: Mateusz Bencer, Paweł Piotrowski");
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}
